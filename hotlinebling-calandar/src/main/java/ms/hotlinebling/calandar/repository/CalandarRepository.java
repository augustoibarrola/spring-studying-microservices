package ms.hotlinebling.calandar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ms.hotlinebling.calandar.entity.Calandar;

public interface CalandarRepository extends JpaRepository<Calandar, Integer> 
{

}
